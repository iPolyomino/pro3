#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "list.h"
#include "myrand.h"

int main(void)
{
	clock_t clk_start, clk_end;

	list_node_t *head_p = create_node(0); /* ヘッダ (ダミー) */

	int n;
	int i;
	int val;
	scanf("%d", &n);

	clk_start = clock();
	myrand_init();
	for(i=0; i<n; i++) {
		list_insert_uniq(head_p,myrand());
	}

	clk_end = clock();
	printf("cpu = %g [sec]\n", (double) (clk_end-clk_start)/CLOCKS_PER_SEC);

	myrand_init();

	clk_start = clock();

	for(i=0; i<n; i++) {
		val = myrand();
		list_find(head_p, val);
	}

	clk_end = clock();
	printf("cpu = %g [sec]\n", (double) (clk_end-clk_start)/CLOCKS_PER_SEC);

	clk_start = clock();

	for(i=0; i<n; i++) {
		val = myrand();
		list_find(head_p, val);
	}

	clk_end = clock();
	printf("cpu = %g [sec]\n", (double) (clk_end-clk_start)/CLOCKS_PER_SEC);

	list_delete(head_p);

	free(head_p);
	return 0;
}
