#include <stdio.h>
#include "image2.h"


void bmp_rotate90(bmp_data_t *bmp);
void bmp_mirror(bmp_data_t *bmp);
void bmp_trans(bmp_data_t *bmp);


int main(void)
{
	bmp_data_t data;

	bmp_read("in.bmp", &data); /* in.bmp から読み込む */
	bmp_rotate90(&data);         /* 処理の呼び出し */
	bmp_write("out.bmp", &data); /* out.bmp への書き出し */

	return 0;
}


/* 画像処理の関数の本体 */
void bmp_rotate90(bmp_data_t *bmp) {
	bmp_trans(bmp);
	bmp_mirror(bmp);
}

void bmp_mirror(bmp_data_t *bmp) {
	int i, j;
	unsigned char tmp;
	for (i=0; i < bmp->header.biHeight; i++) { /* biHeight は画像の高さ */
		for (j=0; j < bmp->header.biWidth / 2; j++) {   /* biWidth  は画像の幅 */
			//r入れ替え
			tmp = bmp->img[i][j].r;
			bmp->img[i][j].r = bmp->img[i][bmp->header.biWidth - j].r;
			bmp->img[i][bmp->header.biWidth - j].r = tmp;
			//g入れ替え
			tmp = bmp->img[i][j].g;
			bmp->img[i][j].g = bmp->img[i][bmp->header.biWidth - j].g;
			bmp->img[i][bmp->header.biWidth - j].g = tmp;
			//b入れ替え
			tmp = bmp->img[i][j].b;
			bmp->img[i][j].b = bmp->img[i][bmp->header.biWidth - j].b;
			bmp->img[i][bmp->header.biWidth - j].b = tmp;
		}
	}
}


void bmp_trans(bmp_data_t *bmp) {
	int i, j;
	unsigned char tmp;
	for (i=0; i < bmp->header.biHeight; i++) { /* biHeight は画像の高さ */
		for (j=i; j < bmp->header.biWidth; j++) { /* biWidth  は画像の幅 */
			//r入れ替え
			tmp = bmp->img[i][j].r;
			bmp->img[i][j].r = bmp->img[j][i].r;
			bmp->img[j][i].r = tmp;
			//g入れ替え
			tmp = bmp->img[i][j].g;
			bmp->img[i][j].g = bmp->img[j][i].g;
			bmp->img[j][i].g = tmp;
			//b入れ替え
			tmp = bmp->img[i][j].b;
			bmp->img[i][j].b = bmp->img[j][i].b;
			bmp->img[j][i].b = tmp;
		}
	}
	unsigned int height = bmp->header.biHeight;
	bmp->header.biHeight = bmp->header.biWidth;
	bmp->header.biWidth = height;
}
