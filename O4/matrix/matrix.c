#include <stdio.h>
#include <assert.h>
#include "matrix.h"


/* 行列の出力 */
void matrix_print(int n, double A[N][N])
{
	int i, j;
	assert(n<=N);
	for (i=0; i<n; i++) {
		for (j=0; j<n; j++) {
			printf(" %9.5f", A[i][j]);
		}
		printf("\n");
	}
}

void matrix_scan(int n, double A[N][N])
{
	int i, j;
	assert(n<=N);
	for (i=0; i<n; i++) {
		for (j=0; j<n; j++) {
			fprintf(stderr, "[%d][%d] ", i, j);
			scanf("%lf", &A[i][j]);
		}
	}
}

void matrix_sum(int n, double A[N][N], double B[N][N], double Y[N][N])
{
	int i, j;
	assert(n<=N);
	for (i=0; i<n; i++) {
		for (j=0; j<n; j++) {
			Y[i][j] = A[i][j] + B[i][j];
		}
	}
}


void matrix_trans2(int n, double A[N][N], double Y[N][N]){
	int i, j;
	assert(n<=N);
	for (i=0; i<n; i++) {
		for (j=0; j<n; j++) {
			Y[i][j] = A[j][i];
		}
	}
}

void matrix_trans1(int n, double A[N][N])
{
	int i, j;
	double tmp;
	assert(n<=N);
	for (i=0; i<n; i++) {
		for (j=i; j<n; j++) {
			tmp = A[i][j];
			A[i][j] = A[j][i];
			A[j][i] = tmp;
		}
	}
}

void vector_scan(int n, double x[N])
{
	int i;
	assert(n<=N);
	for (i=0; i<n; i++) {
		fprintf(stderr, "[%d] ", i);
		scanf("%lf", &x[i]);
	}
}

void vector_print(int n, double x[N])
{
	int i;
	assert(n<=N);
	for (i=0; i<n; i++) {
		printf(" %9.5f\n", x[i]);
	}
}

void matrix_vector_prod(int n, double A[N][N], double x[N], double y[N])
{
	int i, j;
	double sum;
	assert(n<=N);
	for (i=0; i<n; i++) {
		sum = 0;
		for (j=0; j<n; j++) {
			sum += A[i][j] * x[j];
		}
		y[i] = sum;
	}
}

void matrix_copy(int n, double From[N][N], double To[N][N])
{
	int i, j;
	assert(n<=N);
	for (i=0; i<n; i++) {
		for (j=0; j<n; j++) {
			To[i][j] = From[i][j];
		}
	}
}

void vector_copy(int n, double From[N], double To[N])
{
	int i;
	assert(n<=N);
	for (i=0; i<n; i++) {
		To[i] = From[i];
	}
}

void matrix_vector_print(int n, double A[N][N], double B[N])
{
	int i, j;
	assert(n<=N);
	for (i=0; i<n; i++) {
		for (j=0; j<n; j++) {
			printf(" %9.5f", A[i][j]);
		}
		printf(": %9.5f\n", B[i]);
	}
}

void gauss_jordan(int n, double A[N][N], double B[N])
{
	int i, j, k;
	double divide, multiply;
	for (i=0; i<n; i++) {
		//規格化
		divide = A[i][i];
		for (j=0; j<n; j++) {
			A[i][j] /= divide;
		}
		B[i] /= divide;

		//掃き出し
		for (j=0; j<n; j++) {
			if (i == j) {
				continue;
			}
			multiply =  A[j][i] / A[i][i];
			for (k=0; k<n; k++) {
				A[j][k] -= A[i][k] * multiply;
			}
			B[j] -= B[i] * multiply;
		}

		printf("\n<%d行目>\n", i);
		matrix_vector_print(n, A, B);
	}
}
