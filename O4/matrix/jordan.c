#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "matrix.h"

int main (void)
{
	int n;
	double A[N][N];
	double b[N];
	double A_copy[N][N];
	double b_copy[N];
	double y[N];

	/* n の入力 */
	fprintf(stderr, "n = ");
	scanf("%d", &n);
	assert(n<=N);

	/* A と b の入力 */
	fprintf(stderr, "A = \n");
	matrix_scan(n, A);
	fprintf(stderr, "b = \n");
	vector_scan(n, b);
	printf("\n与えられた A と b\n");
	matrix_vector_print(n, A,b);

	/* A と b のコピーを残す */
	matrix_copy(n, A, A_copy);
	vector_copy(n, b, b_copy);

	/* ガウス・ジョルダンの掃き出し */
	gauss_jordan(n, A,b);
	/* A x = b の解 (x) が b に代入される */

	/* 解 x の表示 */
	printf("\n解 x \n");
	vector_print(n, b);

	/* 検算: 元のAと解の積が元の b と等しければ (Ax=bとなっていれば) OK */
	matrix_vector_prod(n, A_copy,b,y);
	printf("\nA x (与えられた b に等しければ OK)\n");
	vector_print(n, y);

	return 0;
}
