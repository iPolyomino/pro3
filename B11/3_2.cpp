#include <iostream>

using namespace std;

class trace {
    private:
        string name;
    public:
        trace(const string& nm) {
            name = nm;
            cerr << name << " begin" << "\n";
        }
        ~trace() {
            cerr << name << " end" << "\n";
        }
};

int add(int a, int b){
    trace t("add");
    return a+b;
}

int main(void) {
    trace t1("main");
    for (int i=0; i<3; i++) {
        trace t2("for-loop");
        int c = add(i, i*i);
    }
    return 0;
}
