#include <iostream>

using namespace std;

class aaa {
    public:
        int x;
        int y;
        aaa() { cout << "aaa()" << "\n";}
        aaa(int xx, int yy) {
            x = xx;
            y = yy;
            cout << "aaa(" << xx << "," << yy << ")" << "\n";
        }
        ~aaa() { cout << "~aaa()" << "\n";}
};

class bbb {
    public:
        int x;
        int y;
        bbb() { cout << "bbb()" << "\n";}
        ~bbb() { cout << "~bbb()" << "\n";}
};

class ccc {
    public:
        aaa p;
        bbb q;
        ccc() {cout << "ccc()" << "\n";}
        ~ccc() { cout << "~ccc()" << "\n";}
};

int main(void) {
    aaa a;
    bbb x[3];
    aaa *p;
    p = new aaa;
    a = aaa(3,4);
    for (int i=0; i<3; i++) {
        aaa x(7,4);
    }
    delete p;
    ccc c;
    return 0;
}
