#include <iostream>

using namespace std;

class Complex {
    private:
        double real;
        double imag;
    public:
        Complex() { real = 0.0; imag = 0.0; }
        Complex(double r) {real = r; imag = 0.0;}
        Complex(double r, double i) {real = r; imag = i;}
        ~Complex() {}
        Complex& operator+=(const Complex&);
        double re() const {return real;}
        double im() const {return imag;}
        void set_re(double r) {real = r;}
        void set_im(double i) {imag = i;}
        void print(ostream& os) const {
            os << real << "+" << imag << "i";
        }
};


Complex operator+(Complex a, Complex b) {
    double r = a.re() + b.re();
    double i = a.im() + b.im();
    return Complex(r, i);
}
Complex operator-(Complex a, Complex b) {
    double r = a.re() - b.re();
    double i = a.im() - b.im();
    return Complex(r, i);
}
Complex operator*(Complex a, Complex b) {
    double r = a.re() * b.re() - a.im() * b.im();
    double i = a.re() * b.im() + a.im() * b.re();
    return Complex(r, i);
}

Complex& Complex::operator+=(const Complex& c) {
    real += c.real;
    imag += c.imag;
    return *this;
}

ostream& operator<<(ostream& os, Complex& c) {
    c.print(os);
    return os;
}

int main(void) {
    Complex a[3];
    for (int i=0; i<3; i++) {
        a[i] = Complex(1.00,2.00);
    }
    a[1] += Complex(1.00,1.00);
    a[2] += Complex(2.00,2.00);
    
    for (int i=0; i<3; i++) cout << a[i] << endl;
    return 0;
}
