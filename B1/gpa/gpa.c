#include <stdio.h>

int calcGpa(int score) {
	if(score < 60) {
		return 0;
	}
	score -= 50;
	if(score == 50) {
		return 4;
	}
	return score/10;
}


int main(void) {
	int i = 0;
	int score;
	int totalGpa = 0;
	int gpa;
	while(1) {
		if(scanf("%d", &score) == EOF) {
			break;
		}
		gpa = calcGpa(score);
		totalGpa += gpa;
		printf("%d\n", gpa);
		i++;
	}
	printf("%0.3f\n", (double)totalGpa/(double)i);
	return 0;
}
