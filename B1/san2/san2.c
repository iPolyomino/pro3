#include <stdio.h>

int judge(int judgeNumber){
	if(judgeNumber % 3 == 0 || judgeNumber % 10 == 3) {
		return 1;
	}
	while(judgeNumber) {
		if(judgeNumber % 10 == 3) {
			return 1;
		}
		judgeNumber /= 10;
	}
	return 0;
}


int main(void) {
	int i;
	int n;
	scanf("%d", &n);
	for(i = 1; i<=n; i++) {
		if(judge(i)) {
			printf("%d\n", i);
		}
	}
	return 0;
}
