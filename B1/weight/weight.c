#include <stdio.h>
#define MAX_I 10

int main(void) {
	int i;
	int n;
	double totalWeight = 0;
	double weight[MAX_I + 1];

	scanf("%d", &n);

	for(i=0; i<n; i++) {
		scanf("%lf", &weight[i]);
	}

	while(1) {
		if(scanf("%d", &i) == EOF) {break;}
		totalWeight += weight[i];
	}

	printf("%.1f\n", totalWeight);

	return 0;
}
