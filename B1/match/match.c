#include <stdio.h>
#define MAX_INPUT 10

int judgeNumber(int tryNumber, int hitNumbers[], int arrayLength){
	int i;
	for(i=0; i<arrayLength; i++) {
		if(tryNumber == hitNumbers[i]) {
			return 1;
		}
	}
	return 0;
}

int main(int argc, char const *argv[]) {
	int m,n;
	int hitNumbers[MAX_INPUT];
	int hitCount;
	int tryNumber;
	int i, j;

	scanf("%d", &n);
	for(i=0; i<n; i++) {
		scanf("%d", &hitNumbers[i]);
	}
	scanf("%d", &m);

	// printf("please input %d numbers.\n", n);
	for(j=0; j<m; j++) {
		hitCount = 0;
		for(i=0; i<n; i++) {
			scanf("%d", &tryNumber);
			hitCount += judgeNumber(tryNumber, hitNumbers, n);
		}
		printf("%d\n", hitCount);
	}

	return 0;
}
