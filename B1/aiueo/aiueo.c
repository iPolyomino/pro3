#include <stdio.h>
#include <string.h>

#define W_LEN 100
#define W_FMT "%100s"

char changeCharacter(char c) {
	char aiu[] = "aiueo";
	int i;
	for(i=0; i<5; i++) {
		if(c==aiu[i]) {
			return c;
		}
	}
	return '*';
}

int main(int argc, char const *argv[]) {
	char input[W_LEN];
	int i;
	while (1) {
		scanf(W_FMT, input);
		if(strcmp(input, "qq") == 0) {
			break;
		}
		for(i=0; input[i]!='\0'; i++) {
			input[i] = changeCharacter(input[i]);
		}
		printf("%s\n", input);
	}
	return 0;
}
