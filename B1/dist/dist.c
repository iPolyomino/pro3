#include <stdio.h>
#include <math.h>

#define REPEAT 3

struct {
	double x;
	double y;
} typedef point_t;

double dist(point_t a, point_t b) {
	double xDiff = a.x - b.x;
	double yDiff = a.y - b.y;
	return sqrt(pow(xDiff, 2) + pow(yDiff, 2));
}

int main(void) {
	point_t aPoint;
	point_t bPoint;
	int i;

	for(i=0; i<REPEAT; i++) {
		scanf("%lf", &aPoint.x);
		scanf("%lf", &aPoint.y);
		scanf("%lf", &bPoint.x);
		scanf("%lf", &bPoint.y);
		printf("%0.4f\n", dist(aPoint, bPoint));
	}
	return 0;
}
