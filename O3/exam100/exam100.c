#include <stdio.h>
#include <stdlib.h>

#define N 100

double myrand(void) {
	static long x = 53402397;

	x = x * 65539 + 125654;
	if(x<0) {
		x += 2147483647;
		x += 1;
	}
	return x / 2147483648.0;
}

int rnd(double m, double s) {
	double ret;
	int rr;
	int i;
	double x = 0.0;

	for(i=0; i<N; i++) {
		x += myrand();
	}
	x -= N / 2;
	ret = s*x/2147483648.0 + m;
	rr = ret + 0.5;

	rr = rr>N ? N : rr;
	rr = rr<0 ? 0 : rr;

	return rr;
}


int main(int argc, char const *argv[]) {
	double average;
	double standardDeviation;
	int examinees;
	int score[21];
	int i;

	for(i=0; i<21; i++)
		score[i] = 0;

	scanf("%lf%lf%d", &average, &standardDeviation, &examinees);

	for(i=0; i<examinees; i++)
		score[rnd(average, standardDeviation/10)/5]++;

	for(i=0; i<21; i++)
		printf("%3d点\t %d\n", i*5, score[i]);

	return 0;
}
