#include <stdio.h>
#include <string.h>

#define POOL_SIZE 512
#define WORD_MAX 32

char pool[POOL_SIZE];
char* word[WORD_MAX];
char* next;

int main(int argc, char const *argv[]) {
    int now_word = 0;
    int i=0, j=0;

    while(1) {
        char one_word[WORD_MAX];
        next = &pool[i];
        word[now_word] = next;
        printf("next word = ");
        if (scanf("%31s", one_word)==EOF) { break; }
        j=0;
        while(one_word[j] != '\0' && j<WORD_MAX) {
            pool[i] = one_word[j];
            i++;
            j++;
        }
        pool[i] = '\0';
        i++;
        now_word++;
    }

    printf("pool\t= (%p)\n", &pool);

    for(i=0, j=0; i<now_word; i++, j++) {
        printf("word[%d]\t= (%p) \"%s\"\n", i, &pool[j], word[i]);
        while(pool[j] != '\0') {
            j++;
        }
    }
    printf("pool\t= (%p)\n", next);

    return 0;
}
