#include <assert.h>
#include <iostream>

class stack {
private:
  int sp;
  int max;
  int *data;

public:
  stack(int sz) {
    sp = 0;
    max = sz;
    data = new int[max];
  }
  ~stack() { delete[] data; }
  void push(int d) {
    assert(sp <= max);
    data[sp++] = d;
  }
  void pop() {
    assert(0 < sp);
    --sp;
  }
  bool empty() const { return sp == 0; }
  int top() const { return data[sp - 1]; }
  int size() const { return sp; }
  void dump(std::ostream &) const;
  stack &operator=(const stack &);
};

void stack::dump(std::ostream &os) const {
  os << "max=" << max << ", ";
  os << "sp=" << sp << ", ";
  os << "data=(";
  for (int i = 0; i < sp; i++)
    os << data[i] << " ";
  os << ")" << std::endl;
}

stack &stack::operator=(const stack &s) {
  if (&s != this) {
    if (max < s.sp) {
      delete[] data;
      data = new int[max = s.max];
    }

    sp = s.sp;
    for (int i = 0; i < sp; i++) {
      data[i] = s.data[i];
    }
  }
  return *this;
}

stack plus(stack s1, stack s2) {
  int sz = s1.size() + s2.size();
  stack tmp(sz), s(sz);
  while (!s2.empty()) {
    tmp.push(s2.top());
    s2.pop();
  }
  while (!s1.empty()) {
    tmp.push(s1.top());
    s1.pop();
  }
  while (!tmp.empty()) {
    s.push(tmp.top());
    tmp.pop();
  }
  return s;
}

class stack_ac : public stack {
private:
  int pop_count;
  int push_count;

public:
  stack_ac(int sz) : stack(sz) {
    pop_count = 0;
    push_count = 0;
  }
  void push(int d) {
    stack::push(d);
    ++push_count;
  }
  void pop() {
    stack::pop();
    ++pop_count;
  }

  int n_push() { return push_count; }
  int n_pop() { return pop_count; }
};

int main(void) {

  stack_ac s(10);

  std::cout << s.n_push() << "\n";
  s.push(12);
  std::cout << s.n_push() << "\n";
  s.push(11);
  std::cout << s.n_push() << "\n";

  std::cout << s.n_pop() << "\n";
  s.pop();
  std::cout << s.n_pop() << "\n";
  s.pop();
  std::cout << s.n_pop() << "\n";

  return 0;
}
