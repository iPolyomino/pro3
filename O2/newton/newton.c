#include <stdio.h>
#include <math.h>
#include "newton.h"

double calc(double a, double b, double c, double x) {
	return a*pow(x,2) + b*x + c;
}

double differential(double a, double b, double c, double x) {
	return 2*a*x + b;
}

double newton(double a, double b, double c, double x0, double eps, int max_repeat) {

	if(max_repeat <= 0 ) {
		return x0;
	}

	double yCoordinate = calc(a, b, c, x0);
	double differentialValue = differential(a, b, c, x0);
	double x1 = x0 - yCoordinate / differentialValue;

	if(fabs(x1 - x0) < eps) {
		return x1;
	}

	return newton(a, b, c, x1, eps, --max_repeat);
}
