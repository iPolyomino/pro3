#include <stdio.h>
#include <stdlib.h>

#define MAX_OMIAI_COUNT 15
#define TRIAL_COUNT 100

//自分がYESというか
int x_accept(int c, int y) {
	if(c<=5 && y<4) {
		return 0;
	}
	return 1;
}

//相手がYESというか
int y_accept(int c, int y, int x){
	if((0.19*x)*(1.1-0.1*y)>(double)(rand()%100)/100.0) {
		return 1;
	}
	return 0;
}

//レベルを生成
int getLevel() {
	int level[] = {1,2,4,2,1};
	int lev = rand() % (1+2+4+2+1);

	int total = 0;
	int i = 0;

	while(total <= lev) {
		total += level[i];
		i++;
	}
	return i;
}

int trial() {
	int myLevel = 1;
	int opponentLevel = 5;

	int i;

	for(i=0; i<MAX_OMIAI_COUNT; i++) {
		int c = i+1;
		if(x_accept(c, opponentLevel) && y_accept(c, opponentLevel, myLevel)) {
			return opponentLevel;
		}
		opponentLevel = getLevel();
	}
	return -5;
}


int main(int argc, char const *argv[]) {
	int i;
	double result = 0;
	for(i=0; i<TRIAL_COUNT; i++) {
		result += trial();
	}
	printf("期待値は%fです．\n", result/TRIAL_COUNT);

	return 0;
}
