#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "iata_db2.h"

/* c が改行記号かどうか判定するマクロ */
#define IS_EOL(c) ((c) == '\n' || (c) == '\r')

/* iata_db.c の内部だけで使用する関数 */
static void extract_data(char line[], char key[], char data[]);
static void record_set(record_t *r, char key[], char data[]);

void db_init(db_t *db)
/* db_t 型変数の初期化 */
{
	int i;
	db->n = 0;
	for (i = 0; i < MAX_RECORDS; i++) {
		strcpy(db->record[i].key, "");
		db->record[i].data = NULL;
		db->record[i].next = NULL;
	}
}

void db_load(db_t *db)
/* db_t 型変数にファイル IATA_F からデータを読み込む */
{
	FILE *fp;
	char line[KEY_LEN + 1 + DATA_LEN + 1];
	char key[KEY_LEN + 1];
	char data[DATA_LEN + 1];

	/* ファイルのオープン */
	if ((fp = fopen(IATA_F, "r")) == NULL) {
		fprintf(stderr, "ファイル (%s) が開けません\n", IATA_F);
		exit(1);
	}

	/* データの読み込み */
	while (fgets(line, DATA_LEN, fp) != NULL) {
		/* 入力データ数が収容可能最大レコード数を超えたらエラー */
		if (MAX_RECORDS <= db->n) {
			fprintf(stderr, "MAX_RECORDS(%d) <= n(%d)\n", MAX_RECORDS, db->n);
			exit(1);
		}
		/* line から key と data を切り出す */
		extract_data(line, key, data);
		/* db->n 番目のレコードに key と data を格納し, db->n を1増やす */
		record_set(&db->record[db->n], key, data);
		db->n++;
	}

	fclose(fp);
}

static void record_set(record_t *r, char key[], char data[])
/* r の指すレコードに key と data を書き込む (data は動的割当て) */
{
	assert(r != NULL);
	assert(key != NULL);
	assert(data != NULL);
	/* key */
	assert(strlen(key) <= KEY_LEN);
	strcpy(r->key, key);
	/* data */
	assert(strlen(data) <= DATA_LEN);
	r->data = (char *)malloc(sizeof(char) * (strlen(data) + 1));
	strcpy(r->data, data);
	r->next = NULL;
}

static void extract_data(char line[], char key[], char data[])
/* line から key と data を抽出する*/
{
	assert(line != NULL);
	assert(key != NULL);
	assert(data != NULL);
	int k;

	/*  ファイルの1行が長過ぎて line に収容できていないことがないかのチェック */
	/*  line の末尾が改行でない → 1行が長過ぎるのでエラー */
	if (!IS_EOL(line[strlen(line) - 1])) {
		fprintf(stderr, "line length exceeded line size\n");
		exit(1);
	}

	/* 先頭の KEY_LEN 文字が key */
	int p_key = 0;
	for (k = 0; k < KEY_LEN; k++) {
		key[k] = line[p_key + k];
	}
	key[k] = '\0';

	/* KEY_LEN+1 文字目以降改行記号までが data */
	int p_data = KEY_LEN + 1;
	for (k = 0; k < DATA_LEN; k++) {
		if (IS_EOL(line[p_data + k])) {
			break;
		}
		data[k] = line[p_data + k];
	}
	data[k] = '\0';
}

void db_dump(db_t *db)
/* db_t 型変数の全データを出力する */
{
	/* 課題 (8-1) : この関数を完成させよ*/
	int i;
	for (i = 0; i < MAX_RECORDS; i++) {
		if (strcmp(db->record[i].key, "") != 0) {
			printf("[%3d]%s %s\n", i, db->record[i].key, db->record[i].data);
		} else {
			printf("[%3d]\n", i);
		}
	}
}

char *db_linear_search(db_t *db, char key[]) {
	int i;
	for (i = 0; i < db->n; i++) {
		if (strcmp(key, db->record[i].key) == 0) {
			return db->record[i].data;
		}
	}
	return NULL;
}

char *db_bi_search(db_t *db, char key[], int low, int high) {
	int mid = (low + high) / 2;

	if (high < low) {
		return NULL;
	}

	if (strcmp(db->record[mid].key, key) == 0) {
		return db->record[mid].data;
	} else if (strcmp(db->record[mid].key, key) < 0) {
		return db_bi_search(db, key, mid + 1, high);
	} else {
		return db_bi_search(db, key, low, mid - 1);
	}
}

char *db_binary_search(db_t *db, char key[]) {
	return db_bi_search(db, key, 0, db->n - 1);
}

record_t *create_record(char key[], char data[]) {
	record_t *new_record;
	new_record = (record_t *)malloc(sizeof(record_t));
	if (new_record == NULL) {
		fprintf(stderr, "節点の割当てに失敗しました\n");
		exit(1);
	}
	record_set(new_record, key, data);
	return new_record;
}

int db_hash_f(char key[]) {
	int i;
	int v = 0;
	for (i = 0; key[i] != '\0'; i++) {
		v = (v << 5) + (key[i] - 'A');
	}
	return v % MAX_RECORDS;
}

void db_hash_load(db_t *db) {
	FILE *fp;
	char line[KEY_LEN + 1 + DATA_LEN + 1];
	char key[KEY_LEN + 1];
	char data[DATA_LEN + 1];
	int hash;
	record_t *record_point = NULL;
	record_t *insert_point = NULL;

	/* ファイルのオープン */
	if ((fp = fopen(IATA_F, "r")) == NULL) {
		fprintf(stderr, "ファイル (%s) が開けません\n", IATA_F);
		exit(1);
	}

	while (fgets(line, DATA_LEN, fp) != NULL) {
		/* 入力データ数が収容可能最大レコード数を超えたらエラー */
		if (MAX_RECORDS <= db->n) {
			fprintf(stderr, "MAX_RECORDS(%d) <= n(%d)\n", MAX_RECORDS, db->n);
			exit(1);
		}

		extract_data(line, key, data);
		record_point = create_record(key, data);
		hash = db_hash_f(key);

		insert_point = &db->record[hash];

		while (insert_point->next != NULL) {
			insert_point = insert_point->next;
		}
		insert_point->next = record_point;

		db->n++;
	}

	fclose(fp);
}

char *db_hash_search(db_t *db, char key[]) {
	int hash = db_hash_f(key);

	record_t *record_point;

	for (record_point = db->record[hash].next; record_point != NULL;
	     record_point = record_point->next) {
		if (strcmp(record_point->key, key) == 0) {
			return record_point->data;
		}
	}

	return NULL;
}
