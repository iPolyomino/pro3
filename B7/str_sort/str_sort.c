#include "str_sort.h"
#include <stdio.h>

void swapmin(int n, record_t *a, int k) {
  int i;
  record_t *min_p;
  record_t tmp;

  min_p = a + k;

  for (i = k; i < n; i++) {
    if (a[i].age < min_p->age) {
      min_p = a + i;
    } else if (a[i].age == min_p->age) {
      if (a[i].height < min_p->height) {
        min_p = a + i;
      }
    }
  }

  tmp = *min_p;
  *min_p = a[k];
  a[k] = tmp;
}

void str_sort(int n, record_t *a) {
  int k;
  for (k = 0; k <= n - 2; k++) {
    swapmin(n, a, k);
  }
}
