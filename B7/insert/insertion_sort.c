void insert(int n, double *a, int k) {
  int i;
  double tmp;
  for (i = k; 0 <= i; i--) {
    if (a[i - 1] <= a[i]) {
      break;
    }
    tmp = a[i];
    a[i] = a[i - 1];
    a[i - 1] = tmp;
  }
}

void sort(int n, double *a) {
  int k;
  for (k = 1; k <= n - 1; k++) {
    insert(n, a, k);
  }
}
