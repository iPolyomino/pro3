#include <stdio.h>

void swapmin(int n, double *a, int k) {
  int i;
  double *min_p;
  double tmp;

  min_p = a + k;

  for (i = k; i < n; i++) {
    min_p = a[i] < *min_p ? a + i : min_p;
  }

  tmp = *min_p;
  *min_p = a[k];
  a[k] = tmp;
}

void sort(int n, double *a) {
  int k;
  for (k = 0; k <= n - 2; k++) {
    swapmin(n, a, k);
  }
}
