#include "string_sort.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void swapmin(int n, char (*a)[STRLEN + 1], int k) {
  int i;
  char *min_p;
  char *tmp = (char *)malloc(sizeof(char) * (STRLEN + 1));

  min_p = a[k];

  for (i = k; i < n; i++) {
    if (strcmp(a[i], min_p) < 0) {
      min_p = a[i];
    }
  }

  strcpy(tmp, min_p);
  strcpy(min_p, a[k]);
  strcpy(a[k], tmp);
  free(tmp);
}

void sort(int n, char (*a)[STRLEN + 1]) {
  int k;
  for (k = 0; k <= n - 2; k++) {
    swapmin(n, a, k);
  }
}

void string_sort(int n, char (*a)[STRLEN + 1]) { sort(n, a); }
