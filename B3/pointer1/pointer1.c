#include <stdio.h>

int main(void)
{
	int a = 1234;
	int b = 6666;
	double x = 111.11;
	double y = 222.22;

	printf("a の番地は %p\n", &a);
	printf("b の番地は %p\n", &b);
	printf("x の番地は %p\n", &x);
	printf("y の番地は %p\n", &y);

	return 0;
}
