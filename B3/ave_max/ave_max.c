#include <stdio.h>
#include "ave_max.h"

int array_scan(int a[]){
	int i;
	int n=0;
	for (i=0; i<MAX; i++) { /* 最大サイズを超えて読まない */
		fprintf(stderr, "a[%d] = ", i);
		if (scanf("%d", &a[i])==EOF) { break; }
		n++;
	}
	return n;
}

int sum(int n, int a[]){
	int sum = 0;
	int i;
	for(i=0; i<n; i++) {
		sum += a[i];
	}
	return sum;
}

int max_number(int n, int a[]){
	int i;
	int max = a[0];
	for(i=1; i<n; i++) {
		max = max < a[i] ? a[i] : max;
	}
	return max;
}

void array_ave_max(int n, int a[], double *ave, int *max){
	*ave = (double)sum(n, a) / (double)n;
	*max = max_number(n, a);
}
