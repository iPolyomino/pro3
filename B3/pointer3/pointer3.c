#include <stdio.h>

int main(void)
{
	int a = 1234;
	int b = 6666;
	double x = 111.11;
	double y = 222.22;

	int *int_p;
	double *double_p;

	printf("a の番地は %p\n", &a);
	printf("b の番地は %p\n", &b);
	printf("x の番地は %p\n", &x);
	printf("y の番地は %p\n", &y);

	int_p = &b;
	double_p = &x;
	*int_p = *int_p + 4;
	*double_p = *double_p * 2;


	while(1) {
		printf("番地を入力 : ");
		scanf("%p",&double_p);
		if (double_p==0) { break; }
		printf("%p 番地の内容は %f\n", double_p, *double_p);
	}

	return 0;
}
