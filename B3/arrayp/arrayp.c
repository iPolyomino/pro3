#include <stdio.h>

int main(int argc, char const *argv[]) {
	int x[] = {0,10,20,30,40};
	int k;
	int i;
	//配列の長さを計算する．
	k = sizeof x / sizeof x[0];

	for(i=0; i<k; i++) {
		printf("%p ", &x[i]);
	}
	printf("\n");

	printf("%p\n", &x);
	for (i = 0; i<k; i++) {
		printf("%p ", &x+i);
	}
	printf("\n");

	for (i = 0; i<k; i++) {
		printf("%d ", *((int*)&x+i));
	}
	printf("\n");

	int *p = &x[2];

	printf("%p, %p\n", &p[2], p+2);

	return 0;
}
