#include <stdio.h>

int main(void)
{
	int a = 1234;
	int b = 6666;
	double x = 111.11;
	double y = 222.22;

	printf("a の番地は %p\n", &a);
	printf("b の番地は %p\n", &b);
	printf("x の番地は %p\n", &x);
	printf("y の番地は %p\n", &y);
/*
   a の番地は 0xffffcc0c
   b の番地は 0xffffcc08
   x の番地は 0xffffcc00
   y の番地は 0xffffcbf8
 */

	printf("\n");
	printf("a の値 = %d\n", *((int*)0xffffcc0c));
	printf("b の値 = %d\n", *((int*)0xffffcc08));
	printf("x の値 = %f\n", *((double*)0xffffcc00));
	printf("y の値 = %f\n", *((double*)0xffffcbf8));

	*((int*)0xffffcc0c) = 1111;
	*((int*)0xffffcc08) = 2010;
	*((double*)0xffffcc00) = 333.33;
	*((double*)0xffffcbf8) = 444.44;

	printf("\n");
	printf("a の値 = %d\n", a);
	printf("b の値 = %d\n", b);
	printf("x の値 = %f\n", x);
	printf("y の値 = %f\n", y);

	return 0;
}
/*
   $ ./a
   a の番地は 0xffffcc0c
   b の番地は 0xffffcc08
   x の番地は 0xffffcc00
   y の番地は 0xffffcbf8

   a の値 = 1234
   b の値 = 6666
   x の値 = 111.110000
   y の値 = 222.220000

   a の値 = 1111
   b の値 = 2010
   x の値 = 333.330000
   y の値 = 444.440000
 */
