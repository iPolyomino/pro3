#include <stdio.h>
#include <math.h>
#include "qeq3.h"

int qeq(double a, double b, double c, double *x1, double *x2){
	double answers[2];
	double d = pow(b, 2) - 4 * a * c;
	int answerCount = 0;
	if(d<0) {
		//No Answer
		answerCount = 0;
	} else if(d==0) {
		//Single Answer
		answers[0] = (-b + sqrt(pow(b, 2)-4*a*c)) / (2*a);
		*x1 = answers[0];
		answerCount = 1;
	} else {
		answers[0] = (-b + sqrt(pow(b, 2)-4*a*c)) / (2*a);
		answers[1] = (-b - sqrt(pow(b, 2)-4*a*c)) / (2*a);
		double large = fabs(answers[0]) > fabs(answers[1]) ? answers[0] : answers[1];
		double small = fabs(answers[0]) <= fabs(answers[1]) ? answers[0] : answers[1];
		*x1 = small;
		*x2 = large;
		answerCount = 2;
	}
	return answerCount;
}
