#include <stdio.h>
#include <stdlib.h>

#define RANGE 100

int irand(int min, int max) {
	return rand()%(max-min+1)+min;
}

int main(int argc, char const *argv[]) {
	int a, b, n;
	int i;

	int frequency[RANGE];

	for(i=0; i<RANGE; i++) {
		frequency[i] = 0;
	}

	printf("a, b, n を入力: ");
	scanf("%d%d%d", &a, &b, &n);

	for(i = 0; i<n; i++) {
		int random = irand(a, b);

		frequency[random-a]++;
		printf("%3d", random);
	}
	printf("\n");

	for(i = 0; i<(b-a+1); i++) {
		printf("%d: %d\n", i+a, frequency[i]);
	}

	return 0;
}
