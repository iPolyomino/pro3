#include <stdio.h>
#include <math.h>
#include "dist3.h"

double dist(double x1, double y1, double x2, double y2) {
	double xDiff = x1 - x2;
	double yDiff = y1 - y2;
	return sqrt(pow(xDiff, 2) + pow(yDiff, 2));
}
