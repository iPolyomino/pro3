#include <stdio.h>
#include "dist3.h"

#define REPEAT 3

int main(void) {
	double x1, y1, x2, y2;
	int i;

	for(i=0; i<REPEAT; i++) {
		scanf("%lf", &x1);
		scanf("%lf", &y1);
		scanf("%lf", &x2);
		scanf("%lf", &y2);
		printf("%0.4f\n", dist(x1, y1, x2, y2));
	}
	return 0;
}
