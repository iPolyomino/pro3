#include <stdio.h>
#include <math.h>

double discriminant(double a, double b, double c) {
	return pow(b, 2) - 4 * a * c;
}

void qeq(double a, double b, double c) {
	double answers[2];
	double d = discriminant(a, b, c);
	if(d<0) {
		//No Answer
		printf("\n");
	} else if(d==0) {
		//Single Answer
		answers[0] = (-b + sqrt(pow(b, 2)-4*a*c)) / (2*a);
		printf("%.5f\n", answers[0]);
	} else {
		answers[0] = (-b + sqrt(pow(b, 2)-4*a*c)) / (2*a);
		answers[1] = (-b - sqrt(pow(b, 2)-4*a*c)) / (2*a);
		double large = fabs(answers[0]) > fabs(answers[1]) ? answers[0] : answers[1];
		double small = fabs(answers[0]) <= fabs(answers[1]) ? answers[0] : answers[1];
		printf("%.5f %.5f\n", small, large);
	}
}

int main(int argc, char const *argv[]) {
	double a,b,c;
	while (1) {
		scanf("%lf%lf%lf\n", &a, &b, &c );
		if(!a) {
			break;
		}
		qeq(a, b, c);
	}
	return 0;
}
