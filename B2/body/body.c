#include <stdio.h>
#include <string.h>
#include <math.h>

#define NM_LEN 31

typedef struct {
	char name[NM_LEN+1]; /* 名前 */
	double stature; /* 身長 [cm] */
	double weight;  /* 重量 [kg] */
	double bmi;     /* BMI値 */
} body_index_t;

double calc_bmi(double stature, double weight) {
	return weight / pow(stature/100.0, 2);
}

void body_index_print(body_index_t x) {
	x.bmi = calc_bmi(x.stature, x.weight);

	printf("name: %s\n", x.name);
	printf("stature: %.1fcm\n", x.stature);
	printf("weight: %.1fkg\n", x.weight);
	printf("BMI: %.1f\n", x.bmi);
}

int main(int argc, char const *argv[]) {
	body_index_t person;
	while (1) {
		scanf("%s%lf%lf", person.name, &person.stature, &person.weight);
		if(strcmp(person.name, "q") == 0) {
			break;
		}
		body_index_print(person);
	}

	return 0;
}
