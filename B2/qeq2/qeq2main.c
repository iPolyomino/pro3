#include <stdio.h>
#include "qeq2.h"

int main(int argc, char const *argv[]) {
	double a,b,c;
	while (1) {
		scanf("%lf%lf%lf\n", &a, &b, &c );
		if(!a) {
			break;
		}
		qeq(a, b, c);
	}
	return 0;
}
