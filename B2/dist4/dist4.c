#include <stdio.h>
#include <math.h>
#include "dist4.h"

double dist(point_t a, point_t b) {
	double xDiff = a.x - b.x;
	double yDiff = a.y - b.y;
	return sqrt(pow(xDiff, 2) + pow(yDiff, 2));
}
