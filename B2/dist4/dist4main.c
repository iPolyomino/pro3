#include <stdio.h>
#include "dist4.h"

#define REPEAT 3

int main(void) {
	point_t aPoint;
	point_t bPoint;
	int i;

	for(i=0; i<REPEAT; i++) {
		scanf("%lf", &aPoint.x);
		scanf("%lf", &aPoint.y);
		scanf("%lf", &bPoint.x);
		scanf("%lf", &bPoint.y);
		printf("%0.4f\n", dist(aPoint, bPoint));
	}
	return 0;
}
