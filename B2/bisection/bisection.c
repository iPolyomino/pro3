#include <stdio.h>
#include <math.h>
#include "bisection.h"

double calc(double x, double a, double b, double c) {
	return a*pow(x,2) + b*x + c;
}


double bisection(double x1, double x2, double a, double b, double c, double eps) {
	double midxCoordinate = (x1 + x2) / 2.0;
	double yCoordinate = calc(midxCoordinate, a, b, c);
	double y1 = calc(x1, a, b, c);
	double y2 = calc(x2, a, b, c);

	if(fabs(y1) < eps) {
		return x1;
	} else if(fabs(y2) < eps) {
		return x2;
	} else if(fabs(x1-x2) < eps) {
		return x1;
	}

	double small = yCoordinate <= 0 ? midxCoordinate : x1;
	double large = yCoordinate > 0 ? midxCoordinate : x2;

	if(y1 > y2) {
		small = yCoordinate >= 0 ? midxCoordinate : x1;
		large = yCoordinate < 0 ? midxCoordinate : x2;
	}

	return bisection(small, large, a, b, c, eps);
}
