#include <stdio.h>
#include <math.h>

#define REPEAT 3

double dist(double x1, double y1, double x2, double y2) {
	double xDiff = x1 - x2;
	double yDiff = y1 - y2;
	return sqrt(pow(xDiff, 2) + pow(yDiff, 2));
}

int main(void) {
	double x1, y1, x2, y2;
	int i;

	for(i=0; i<REPEAT; i++) {
		scanf("%lf", &x1);
		scanf("%lf", &y1);
		scanf("%lf", &x2);
		scanf("%lf", &y2);
		printf("%0.4f\n", dist(x1, y1, x2, y2));
	}
	return 0;
}
