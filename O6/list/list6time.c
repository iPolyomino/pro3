#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "list.h"

static unsigned long myrand_r;

#define R_INIT 53402397UL
#define A 65539UL
#define B 125654UL
#define MASK 0x7fffffffUL

void myrand_init( void )
{
	myrand_r = R_INIT;
}

int myrand( void )
{
	myrand_r = (myrand_r * A + B) & MASK;
	return (int) myrand_r;
}

int main(void)
{
	int n;
	int i;

	clock_t clk_start, clk_end;

	list_node_t *head_p = create_node(0); /* ヘッダ (ダミー) */

	scanf("%d", &n);

	clk_start = clock();

	myrand_init();
	for(i=0; i<n; i++) {
		list_insert(head_p, myrand());
	}

	myrand_init();
	for(i=0; i<n; i++) {
		list_insert_delete_dup(head_p, myrand());
	}

	clk_end = clock();
	printf("cpu = %g [sec]\n", (double) (clk_end-clk_start)/CLOCKS_PER_SEC);

	clk_start = clock();

	myrand_init();
	for(i=0; i<n; i++) {
		list_insert(head_p, myrand());
	}

	myrand_init();
	for(i=0; i<n; i++) {
		list_insert_delete_dup2(head_p, myrand());
	}

	clk_end = clock();
	printf("cpu = %g [sec]\n", (double) (clk_end-clk_start)/CLOCKS_PER_SEC);

	list_delete(head_p);

	free(head_p);
	return 0;
}
