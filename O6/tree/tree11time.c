#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "tree.h"

static unsigned long myrand_r;

#define R_INIT 53402397UL
#define A 65539UL
#define B 125654UL
#define MASK 0x7fffffffUL

void myrand_init( void )
{
    myrand_r = R_INIT;
}

int myrand( void )
{
    myrand_r = (myrand_r * A + B) & MASK;
    return (int) myrand_r;
}

int main (void)
{
    clock_t clk_start, clk_end;

    tree_node_t *root = NULL;

    int n;
    int i;
    int val;
    scanf("%d", &n);

    clk_start = clock();

    for(i=0; i<n; i++) {
        int data = myrand();
        tree_node_t *p = tree_find(root,data);
        root = tree_insert_uniq(root,data);
    }

    myrand_init();

    for(i=0; i<n; i++) {
        tree_find(root, myrand());
    }
    
    for(i=0; i<n; i++) {
        tree_find(root, myrand());
    }

    clk_end = clock();
    printf("cpu = %g [sec]\n", (double) (clk_end-clk_start)/CLOCKS_PER_SEC);

    //printf("[");
    //tree_print(root);
    //printf(" ]\n");

    tree_delete(root); 
    return 0;
}
