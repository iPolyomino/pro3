#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

class Entry {
    public:
        std::string name;
        std::string phone;
        Entry(const std::string &nm = "", const std::string &ph = "") {
            name = nm;
            phone = ph;
        }
};

class by_name {
    public:
        bool operator()(const Entry& e1, const Entry& e2) const {
            return e1.name < e2.name;
        }
};

class by_phone {
    public:
        bool operator()(const Entry& e1, const Entry& e2) const {
            return e1.phone < e2.phone;
        }
};

std::ostream &operator<<(std::ostream &os, const Entry &e) {
    os << e.name << ": " << e.phone;
    return os;
}

int main(void) {
    std::vector<Entry> e;

    e.push_back(Entry("Kwansei Gakuin University (PR)", "0798-54-6017"));
    e.push_back(Entry("Kwansei Gakuin University (KSC)", "079-565-7600"));
    e.push_back(Entry("Kobe University", "078-881-1212"));
    e.push_back(Entry("Sanda Woodytown SATY", "079-564-8800"));
    e.push_back(Entry("Sanda Hotel", "079-564-1101"));

    sort(e.begin(), e.end(), by_name());
    for (auto it : e) 
        std::cout << it << "\n";

    sort(e.begin(), e.end(), by_phone());
    //for (auto it : e) 
        //std::cout << it << "\n";
        

    for (int i = 0; i < e.size(); i++) {
        // ------------------------------------------------
        // 電話番号 "079-xxx-xxxx" を "xxx-xxxx" にせよ
        // ------------------------------------------------
        if (e[i].phone.find("079-") == e[i].phone.npos) continue;
        e[i].phone.replace(e[i].phone.find("079-"), 4, "");
    }

    std::cout << std::endl;
    std::cout << "検索用文字列を入力して下さい: ";
    std::string s;
    std::cin >> s;
    // ------------------------------------------------
    //  name に s を含むデータを全て表示せよ
    // ------------------------------------------------

    for (int i = 0; i < e.size(); i++) {
        if (e[i].name.find(s) != e[i].name.npos) {
            std::cout << e[i] << "\n";
        }
    }

    return 0;
}
