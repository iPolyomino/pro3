#include <iostream>
#include <string>
#include <map>

int main(void) {
    std::map<std::string, std::string> numbers;

    numbers["Kwansei Gakuin University (PR)"] = "0798-54-6017";
    numbers["Kwansei Gakuin University (KSC)"] = "079-565-7600";
    numbers["Kobe University"] = "078-881-1212";
    numbers["Sanda Woodytown SATY"] = "079-564-8800";
    numbers["Sanda Hotel"] = "079-564-1101";
    
    for (auto&& mp : numbers) {
        if (mp.second.find("079-") == mp.second.npos) continue;
        mp.second.replace(mp.second.find("079-"), 4, "");
    }

    std::cout << std::endl;
    std::cout << "検索用文字列を入力して下さい: ";
    std::string s;
    std::cin >> s;
    // ------------------------------------------------
    //  name に s を含むデータを全て表示せよ
    // ------------------------------------------------
    
    for (auto mp : numbers) {
        if (mp.first.find(s) != mp.first.npos) {
            std::cout << mp.first << ": " << mp.second << "\n";
        }
    }
    
    return 0;
}
