#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

typedef std::list<int> int_list;
typedef int_list::iterator int_list_iter;
typedef std::vector<int> int_vector;
typedef int_vector::iterator int_vec_iter;


int main(void) {
    // 整数リスト li = (3 5 2 3 2 3) を作る
    int_vector li;
    li.push_back(3);
    li.push_back(5);
    li.push_back(2);
    li.push_back(3);
    li.push_back(2);
    li.push_back(3);

    std::cout << ">";
    int i;
    std::cin >> i;

    // リスト中の i を検索
    int_vec_iter p = find(li.begin(), li.end(), i);
    if (p == li.end()) {
        // 見つからない場合
        std::cout << "not found\n";
    } else {
        // 見つかった場合
        std::cout << *p << " found\n";

        // 見つかったところの直後から更に検索を継続
        p = find(++p, li.end(), i);
        if (p == li.end()) {
            std::cout << "not found\n";
        } else {
            std::cout << *p << " found again\n";
        }
    }

    return 0;
}
