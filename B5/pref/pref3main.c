#include <stdio.h>
#include <stdlib.h>
#include "pref.h"

#define PREF_MAX 47

int main(void)
{
	int i, n = 0;
	pref_t *p[PREF_MAX];
	pref_t *new_pref = NULL;

	while ((new_pref = pref_new_scan()) != NULL) {
		p[n] = pref_new(new_pref->name, new_pref->area, new_pref->population);
		free(new_pref);
		new_pref = NULL;
		n++;
	}

	pref_reverse(p, n);

	for(i=0; i<n; i++) {
		pref_print(p[i]);
	}

	for(i=0; i<n; i++) {
		free(p[i]);
		p[i] = NULL;
	}

	return 0;
}
