#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pref.h"


pref_t *pref_new(char *name, double area, int population) {
	pref_t *new_pref = NULL;
	new_pref = (pref_t *) malloc(sizeof(pref_t));
	strncpy(new_pref->name, name, NAMELEN);
	new_pref->area = area;
	new_pref->population = population;
	return new_pref;
}

void pref_print(pref_t* p) {
	printf("%-10s %9.2f %9d\n", p->name, p->area, p->population);
}

pref_t *pref_new_scan(){
	pref_t *new_pref = NULL;
	new_pref = (pref_t *) malloc(sizeof(pref_t));
	if (scanf("%s %lf %d", new_pref->name, &new_pref->area, &new_pref->population) == EOF) {
		return NULL;
	}

	return new_pref;
}

void pref_reverse(pref_t *p[], int n) {
	pref_t *tmp;
	int i;
	for(i=0; i<n/2; i++) {
		tmp = p[i];
		p[i] = p[n-1-i];
		p[n-1-i] = tmp;
	}
}
