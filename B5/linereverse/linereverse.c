#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINELEN 128

void reverse(int n, char *line[]){
	char *tmp;
	int i;
	for(i=0; i<n/2; i++) {
		tmp = line[i];
		line[i] = line[n-1-i];
		line[n-1-i] = tmp;
	}
}

char *save_phrase(char phrase[]){
	int length = 0;
	int i;
	char *str_p = NULL;

	while(phrase[length]!='\0') {
		length++;
	}
	str_p = malloc(sizeof(char) * (length + 1));
	for(i=0; i<length; i++) {
		str_p[i] = phrase[i];
	}
	str_p[length] = '\0';
	return str_p;
}

int main(void)
{
	char s[LINELEN+1];
	char *line[LINELEN];
	int line_count = 0;
	int i;

	while (fgets(s, LINELEN, stdin) != NULL) {
		line[line_count] = save_phrase(s);
		line_count++;
	}
	reverse(line_count, line);
	for(i=0; i<line_count; i++) {
		printf("%s", line[i]);
		free(line[i]);
		line[i] = NULL;
	}

	return 0;
}
