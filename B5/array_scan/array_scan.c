#include <stdio.h>
#include <stdlib.h>

int* new_array_scan(int *pn) {
	int i;

	scanf("%d", pn);
	int *array_p;
	array_p = malloc(sizeof(int) * *pn);

	for (i=0; i<*pn; i++) {
		scanf("%d", array_p+i);
	}

	return array_p;
}
