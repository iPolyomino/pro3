#include <stdio.h>
#include <stdlib.h>

int* new_array_scan(int *pn);
void min_max(int *data, int n);
void print_distr(int *data, int min, int count);
void distr(int *pn, int *input_array);

int main(void)
{
    int *a = NULL;
    int n;

    a = new_array_scan(&n);

    distr(&n, a);

    free(a);
    a = NULL;

    return 0;
}
void distr(int *pn, int *input_array) {
	int i;
	int min = input_array[0];
	int max = input_array[0];
	for(i=0; i<*pn; i++) {
		min = input_array[i] < min ? input_array[i] : min;
		max = input_array[i] > max ? input_array[i] : max;
	}
	int *mem = NULL;
	mem = malloc(sizeof(int) * (max - min + 1));

	if (mem==NULL) {
		fprintf(stderr, "mem のmalloc が失敗\n");
		exit(1);
	}

	for(i=0; i<*pn; i++) {
		mem[i] = 0;
	}

	for(i=0; i<*pn; i++) {
		mem[input_array[i] - min]++;
	}
	print_distr(mem, min, (max - min + 1));
}


void print_distr(int *data, int min, int count) {
	int i;
	for(i=0; i<count; i++) {
		if(data[i] != 0) {
			printf("%d: %d\n", min+i, data[i]);
		}
	}
}
