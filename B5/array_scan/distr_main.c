#include <stdio.h>
#include <stdlib.h>

int* new_array_scan(int *pn);
int* distr(int *pn, int *input_array);

int main(void)
{
    int *a = NULL;
    int n;

    a = new_array_scan(&n);

    distr(&n, a);

    free(a);
    a = NULL;

    return 0;
}
