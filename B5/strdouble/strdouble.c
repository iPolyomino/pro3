#include <stdio.h>
#include <stdlib.h>
#define SLEN 126
#define SFMT "%126s"

char *strdouble(char s[]){
    int length = 0;
    int i, j;
    char *str_p = NULL;

    while(s[length]!='\0') {
        length++;
    }
    //2倍の文字とNULL文字
    str_p  = malloc(sizeof(char) * (length * 2 + 1));

    for(j=0; j<2; j++) {
        for(i=0; i<length; i++) {
            str_p[length*j+i] = s[i];
        }
    }
    str_p[length * 2] = '\0';
    return str_p;
}
