#include <stdio.h>
#include <stdlib.h>
#include "istack.h"

istack_t *istack_new(int size) {
	istack_t *new_data = NULL;
	new_data = (istack_t * )malloc(sizeof(istack_t));

	if(new_data==NULL) {
		fprintf(stderr, "new_data のmalloc が失敗\n");
		exit(1);
	}

	int *data_p = NULL;

	data_p = (int * )malloc(sizeof(int) * size);

	if(data_p==NULL) {
		fprintf(stderr, "data_p のmalloc が失敗\n");
		exit(1);
	}

	new_data->size = size;
	new_data->sp = 0;
	new_data->data = data_p;

	return new_data;
}

void istack_delete(istack_t *s) {
	free(s->data);
	s->data = NULL;
	free(s);
	s = NULL;
}

void istack_push(istack_t *s, int d) {
	if(s->sp >= s->size) {
		s->size *= 2;
		s->data = realloc(s->data, sizeof(int) * s->size);
	}
	s->data[s->sp] = d;
	s->sp++;
}

int istack_pop(istack_t *s) {
	if(s->sp == 0) {
		return 0;
	}
	s->sp--;
	return s->data[s->sp];
}

int istack_empty(istack_t *s) {
	return s->sp == 0 ? 1 : 0;
}

int istack_full(istack_t *s) {
	return s->sp == s->size ? 1 : 0;
}
