#include <stdio.h>
#include "matrix.h"

int main(void)
{
	int n;
	double A[N][N], X[N], Y[N];

	fprintf(stderr, "n = ");
	scanf("%d", &n);
	fprintf(stderr, "A\n");
	matrix_scan(n, A);
	fprintf(stderr, "X\n");
	vector_scan(n, X);

	matrix_vector_prod(n, A, X, Y);

	printf("A\n");
	matrix_print(n, A);
	printf("x\n");
	vector_print(n, X);
	printf("y\n");
	vector_print(n, Y);

	return 0;
}
