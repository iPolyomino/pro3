#include <stdio.h>
#include <stdlib.h>
#include "image1.h"


void bmp_collapse(bmp_header_t *hd, pixel_t img[MAX_Y][MAX_X]);


int main(void)
{
	bmp_header_t hd;
	pixel_t img [MAX_Y][MAX_X];

	bmp_read("in.bmp", &hd, img); /* in.bmp から読み込む */
	bmp_collapse(&hd, img);         /* 処理の呼び出し */
	bmp_write("out.bmp", &hd, img); /* out.bmp への書き出し */

	return 0;
}


/* 画像処理の関数の本体 */
void bmp_collapse(bmp_header_t *hd, pixel_t img[MAX_Y][MAX_X])
{
	int i, j;

	hd->biHeight /= 2;
	for (i=0; i< hd->biHeight; i++) { /* biHeight は画像の高さ */
		for (j=0; j< hd->biWidth; j++) { /* biWidth  は画像の幅 */
			img[i][j].r = (img[i*2][j].r + img[i*2+1][j].r) / 2;
			img[i][j].g = (img[i*2][j].g + img[i*2+1][j].g) / 2;
			img[i][j].b = (img[i*2][j].b + img[i*2+1][j].b) / 2;
		}
	}
}
