#include <stdio.h>
#include "image2.h"


void bmp_collapse(bmp_data_t *data);


int main(void)
{
	bmp_data_t data;

	bmp_read("in.bmp", &data); /* in.bmp から読み込む */
	bmp_collapse(&data);         /* 処理の呼び出し */
	bmp_write("out.bmp", &data); /* out.bmp への書き出し */

	return 0;
}


/* 画像処理の関数の本体 */
void bmp_collapse(bmp_data_t *bmp) {
	int i, j;

	bmp->header.biHeight /= 2;
	for (i=0; i< bmp->header.biHeight; i++) { /* biHeight は画像の高さ */
		for (j=0; j< bmp->header.biWidth; j++) { /* biWidth  は画像の幅 */
			bmp->img[i][j].r = (bmp->img[i*2][j].r + bmp->img[i*2+1][j].r) / 2;
			bmp->img[i][j].g = (bmp->img[i*2][j].g + bmp->img[i*2+1][j].g) / 2;
			bmp->img[i][j].b = (bmp->img[i*2][j].b + bmp->img[i*2+1][j].b) / 2;
		}
	}
}
