#include <stdio.h>
#include "image1.h"


void bmp_mosaic(bmp_header_t *hd, pixel_t img[MAX_Y][MAX_X]);


int main(void)
{
	bmp_header_t hd;
	pixel_t img [MAX_Y][MAX_X];

	bmp_read("in.bmp", &hd, img); /* in.bmp から読み込む */
	bmp_mosaic(&hd, img);         /* 処理の呼び出し */
	bmp_write("out.bmp", &hd, img); /* out.bmp への書き出し */

	return 0;
}


/* 画像処理の関数の本体 */
void bmp_mosaic(bmp_header_t *hd, pixel_t img[MAX_Y][MAX_X])
{
	int i, j, k, l;
	int sum;
	unsigned char average;

	for (i=0; i< hd->biHeight; i+=8) { /* biHeight は画像の高さ */
		for (j=0; j< hd->biWidth; j+=8) { /* biWidth  は画像の幅 */
			//8px * 8pxの合計を出す
			if (i > hd->biHeight / 2) {
				img[i][j].r = img[i][j].r;
				img[i][j].g = img[i][j].g;
				img[i][j].b = img[i][j].b;
				continue;
			}
			sum = 0;
			for (k=i; k<i+8; k++) {
				for (l=j; l<j+8; l++) {
					sum += img[k][l].r;
				}
			}
			average = sum / (8*8);

			for (k=i; k<i+8; k++) {
				for (l=j; l<j+8; l++) {
					img[k][l].r = average;
				}
			}

			sum = 0;
			for (k=i; k<i+8; k++) {
				for (l=j; l<j+8; l++) {
					sum += img[k][l].g;
				}
			}
			average = sum / (8*8);
			for (k=i; k<i+8; k++) {
				for (l=j; l<j+8; l++) {
					img[k][l].g = average;
				}
			}

			sum = 0;
			for (k=i; k<i+8; k++) {
				for (l=j; l<j+8; l++) {
					sum += img[k][l].b;
				}
			}
			average = sum / (8*8);
			for (k=i; k<i+8; k++) {
				for (l=j; l<j+8; l++) {
					img[k][l].b = average;
				}
			}
		}
	}
}
