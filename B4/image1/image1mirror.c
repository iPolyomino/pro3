#include <stdio.h>
#include "image1.h"


void bmp_mirror(bmp_header_t *hd, pixel_t img[MAX_Y][MAX_X]);


int main(void)
{
	bmp_header_t hd;
	pixel_t img [MAX_Y][MAX_X];

	bmp_read("in.bmp", &hd, img); /* in.bmp から読み込む */
	bmp_mirror(&hd, img);         /* 処理の呼び出し */
	bmp_write("out.bmp", &hd, img); /* out.bmp への書き出し */

	return 0;
}


/* 画像処理の関数の本体 */
void bmp_mirror(bmp_header_t *hd, pixel_t img[MAX_Y][MAX_X])
{
	int i, j;
	unsigned char tmp;
	for (i=0; i< hd->biHeight; i++) { /* biHeight は画像の高さ */
		for (j=0; j< hd->biWidth / 2; j++) {   /* biWidth  は画像の幅 */
			//r入れ替え
			tmp = img[i][j].r;
			img[i][j].r = img[i][hd->biWidth - j].r;
			img[i][hd->biWidth - j].r = tmp;
			//g入れ替え
			tmp = img[i][j].g;
			img[i][j].g = img[i][hd->biWidth - j].g;
			img[i][hd->biWidth - j].g = tmp;
			//b入れ替え
			tmp = img[i][j].b;
			img[i][j].b = img[i][hd->biWidth - j].b;
			img[i][hd->biWidth - j].b = tmp;
		}
	}
}
