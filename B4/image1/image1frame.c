#include <stdio.h>
#include <stdlib.h>
#include "image1.h"


void bmp_frame(bmp_header_t *hd, pixel_t img[MAX_Y][MAX_X]);


int main(void)
{
	bmp_header_t hd;
	pixel_t img [MAX_Y][MAX_X];

	bmp_read("in.bmp", &hd, img); /* in.bmp から読み込む */
	bmp_frame(&hd, img);         /* 処理の呼び出し */
	bmp_write("out.bmp", &hd, img); /* out.bmp への書き出し */

	return 0;
}


/* 画像処理の関数の本体 */
void bmp_frame(bmp_header_t *hd, pixel_t img[MAX_Y][MAX_X])
{
	int i, j;

	for (i=0; i< hd->biHeight; i++) { /* biHeight は画像の高さ */
		for (j=0; j< hd->biWidth; j++) { /* biWidth  は画像の幅 */
			img[i][j].r = img[i][j].r;
			img[i][j].g = img[i][j].g;
			img[i][j].b = img[i][j].b;
			if(i < 16 ||   i > hd->biHeight - 16 || j < 16 || j > hd->biWidth - 16) {
				img[i][j].r = 251;
				img[i][j].g = 150;
				img[i][j].b = 0;
			}
		}
	}

}
