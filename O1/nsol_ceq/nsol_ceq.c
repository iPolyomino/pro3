#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double discriminant(double a, double b, double c) {
	return pow(b, 2) - 4 * a * c;
}

double *solv(double a, double b, double c) {
	double *answer = malloc(sizeof(double) * 2);
	answer[0] = (-b + sqrt(pow(b, 2)-4*a*c)) / (2*a);
	answer[1] = (-b - sqrt(pow(b, 2)-4*a*c)) / (2*a);
	return answer;
}

int main(int argc, char const *argv[]) {
	double a,b,c,d;
	double *answer;
	int i;

	printf("please input numbers\n");
	printf("f(x) = ax^3 + bx^2+ cx + d\n");
	scanf("%lf%lf%lf%lf", &a, &b, &c, &d);

	while(a!=0) {
		//f(x) = ax^3 + bx^2+ cx + d
		//f'(x) = 3ax^2 + 2bx+ c
		double da = 3*a;
		double db = 2*b;
		double dc = c;
		double discriminantValue = discriminant(da, db, dc);

		//D<=0 -> 1
		if(discriminantValue <= 0) {
			printf("1\n");
		} else {
			answer = solv(da, db, dc);
			// printf("%f %f\n", answer[0], answer[1]);
			double yCoordinate[2];
			for(i=0; i<2; i++) {
				yCoordinate[i] = a*pow(answer[i], 3)+b*pow(answer[i], 2)+c*answer[i]+d;
			}
			free(answer);

			if(yCoordinate[0]*yCoordinate[1] > 0) {
				printf("1\n");
			} else if(yCoordinate[0]*yCoordinate[1] == 0) {
				printf("2\n");
			} else {
				printf("3\n");
			}
		}
		printf("please input numbers\n");
		printf("f(x) = ax^3 + bx^2+ cx + d\n");
		scanf("%lf%lf%lf%lf",&a,&b,&c,&d);
	}
	return 0;
}
