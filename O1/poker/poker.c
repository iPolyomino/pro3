#include <stdio.h>

#define CARD_COUNT 5
#define MESSAGE_LENGTH 20
#define CARD_KIND 13

void judge(int inputNumbers[]);
int isFourCard(int frequency[]);
int isFullHouse(int frequency[]);
int isThreeCard(int frequency[]);
int countPair(int frequency[]);

int main(int argc, char const *argv[]) {
	int input[CARD_COUNT];
	int i;

	while (1) {
		for(i=0; i<CARD_COUNT; i++) {
			if(scanf("%d", &input[i]) == EOF) {
				return 0;
			}
		}
		judge(input);
	}

	return 0;
}


void judge(int inputNumbers[]) {
	int frequency[CARD_KIND];
	int i;

	for(i=0; i<CARD_KIND; i++) {
		frequency[i] = 0;
	}

	for(i=0; i<CARD_COUNT; i++) {
		frequency[inputNumbers[i]-1]++;
	}

	if(isFourCard(frequency)) {
		printf("four of a kind\n");
		return;
	}
	if(isFullHouse(frequency)) {
		printf("full house\n");
		return;
	}
	if(isThreeCard(frequency)) {
		printf("three of a kind\n");
		return;
	}
	if(countPair(frequency) == 2) {
		printf("two pair\n");
		return;
	}
	if(countPair(frequency) == 1) {
		printf("one pair\n");
		return;
	}
	printf("no pair\n");
}

int isFourCard(int frequency[]) {
	int i;
	for(i=0; i<CARD_KIND; i++) {
		if(frequency[i]>=4) {
			return 1;
		}
	}
	return 0;
}

int isFullHouse(int frequency[]) {
	int i;
	for(i=0; i<CARD_KIND; i++) {
		if(frequency[i] != 0 && frequency[i] != 2 && frequency[i] != 3) {
			return 0;
		}
	}
	return 1;
}

int isThreeCard(int frequency[]) {
	int i;
	for(i=0; i<CARD_KIND; i++) {
		if(frequency[i] == 3) {
			return 1;
		}
	}
	return 0;
}

int countPair(int frequency[]){
	int pairCount = 0;
	int i;
	for(i=0; i<CARD_KIND; i++) {
		if(frequency[i] == 2) {
			pairCount++;
		}
	}
	return pairCount;
}
