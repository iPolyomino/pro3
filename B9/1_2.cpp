#include <bits/stdc++.h>

using namespace std;

void swap(int &x, int &y) {
    int tmp = x;
    x = y;
    y = tmp;
}

int main(void) {
    int a, b;
    a = 1;
    b = 2;

    swap(a, b);

    cout << "a is " << a << endl;
    cout << "b is " << b << endl;
    return 0;
}
