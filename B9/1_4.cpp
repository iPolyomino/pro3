#include <iostream>
#include <assert.h>

using namespace std;

class stack {
    private:
        int max;
        int *data;
        int sp;
    public:
        void push(int d){assert(sp<max); data[sp++] = d;};
        void pop() {assert(0<sp); --sp;};
        bool empty() const {return sp==0;};
        int top() const {return data[sp-1];};
        int size() const {return sp;};
        stack(int sz = 100) {
            sp = 0;
            max = sz;
            data = new int[max];
            cout << "the max is " << max << endl;
        };
        ~stack() {delete [] data;};
};

int main(void) {
    stack s;
    s.push(5);
    s.push(8);
    s.push(9);
    cout << s.top() << endl;
    s.pop();
    cout << s.top() << endl;
    cout << s.size() << endl;
    while(!s.empty()) {
        cout << s.top() << endl;
        s.pop();
    }
    return 0;
}
