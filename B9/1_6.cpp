#include <iostream>

using namespace std;

class Complex {
    private:
        double real;
        double imag;
    public:
        Complex() { real = 0.0; imag = 0.0; }
        Complex(double r) {real = r; imag = 0.0;}
        Complex(double r, double i) {real = r; imag = i;}
        ~Complex() {}
        double re() const {return real;}
        double im() const {return imag;}
        void set_re(double r) {real = r;}
        void set_im(double i) {imag = i;}
        void print(ostream& os) const {
            os << real << "+" << imag << "i";
        }
};

int main(void) {
    Complex a;
    a.set_re(1.11);
    a.set_im(2.22);
    Complex b(3.33, 4.44);
    Complex c;
    c = Complex(9.99, 8.88);
    Complex d = Complex(5.55);

    a.print(cout); cout << endl;
    b.print(cout); cout << endl;
    c.print(cout); cout << endl;
    d.print(cout); cout << endl;

    c = a;
    c.print(cout); cout << endl;

    return 0;
}
