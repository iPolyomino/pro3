#include <iostream>
#include <assert.h>

using namespace std;

class stack {
    private:
        int max;
        int *data;
        int sp;
    public:
        void push(int d){assert(sp<max); data[sp++] = d;};
        void pop() {assert(0<sp); --sp;};
        bool empty() const {return sp==0;};
        int top() const {return data[sp-1];};
        int size() const {return sp;};
        friend bool equal(const stack&, const stack&);
        stack(int sz = 100) {
            sp = 0;
            max = sz;
            data = new int[max];
        };
        ~stack() {delete [] data;};
};

bool equal(const stack& s1, const stack& s2) {
    bool eq = true;
    if (s1.sp != s2.sp) {
        eq = false;
    } else {
        for (int i = 0; i < s1.sp && eq; i++) {
            if (s1.data[i]!=s2.data[i]) {
                eq = false;
            }
        }
    }
    return eq;
}

int main(void) {
    stack s1, s2;
    s1.push(5);
    s1.push(8);
    s1.push(9);
    s2.push(5);
    s2.push(8);
    s2.push(9);
    s2.push(10);
    cout << equal(s1, s2) << endl;
    s2.pop();
    cout << equal(s1, s2) << endl;

    return 0;
}
